import fs from 'fs'
import multer from 'multer'

module.exports = (req, res, next) => {
	

	const config = {
		storage:multer.diskStorage({
			destination: function (req,file,next){
				console.log(req);
				next(null,'../upload/produk')
			},
			filename: function (req,file,next){
				const ext = file.mimetype.split('/')[1]
				next(null,file.fieldname + '-' + Date.now() + '.' + ext)			
			}
		}),
		fileFilter: function (req,file,next){
			console.log(req);
			if(!file){
				next()
			}
			const image = file.mimetype.startsWith('image/')
			if(image){
				next(null,true)
			}else{
				console.log('tipe file tdk didukung')
				// next()
			}
		}
	}

	const upload = multer(config);
	// return upload.single('upl')
	// console.log(upload)

}
// cek ekstensi


// const upload = multer({ dest: '../upload/produk' })