//middleware/common.js
var multer = require('multer');


Middleware = {
        //Checks whether user is logged in
    isAuthenticated: function(req,res,next){
            if(req.isAuthenticated()){
                return next();
            }
            req.flash('auth',"You do not have the proper permissions to access this page");
            res.redirect('/');
        },
    multerSetup: function(req,res,next){
        var storage = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, './public/uploads/')
            },
            //detects file type, then adds extension to match file type
            filename: function (req, file, cb) {
                var ext = "";
                switch(file.mimetype){
                    case 'image/png':
                        ext = '.png';
                        break;
                    case 'image/jpeg':
                        ext = '.jpeg';
                        break;
                    default:
                        ext = '';
                }
                cb(null, Date.now() + ext); //Appending .jpg
            }
        });


        var upload = multer({storage:storage, fileFilter: function (req, file, cb) {
            var acceptedExt = ['.png','.jpg','.gif','.bmp'];
            if (req.hasOwnProperty('file') && acceptedExt.indexOf(path.extname(file.originalname))=== -1) {
                return cb(new Error('Image type not allowed: ' + path.extname(file.originalname)));
            }

            cb(null, true)
        }});

        return upload.single('upl');
     }

};

module.exports = Middleware;


// routes/index.js
var middleware = require('../middleware/common');
var isAuthenticated = middleware.isAuthenticated;
var upload = middleware.multerSetup;
var upload = middleware.multerSetup();
...

router.post('/updateuser',
    upload,
...,
    function (req, res, next) {
        res.redirect('/dashboard');
    }
);