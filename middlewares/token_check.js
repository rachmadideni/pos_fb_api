const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
	let token_secret = process.env.TOKEN_SECRET
	let user_token = req.body.token || req.query.token || req.headers['x-access-token']
	if (user_token) {
		jwt.verify(user_token,token_secret,(err, decoded) => {
			if (err) {
				return res.json({
					success:false,
					message:'Terjadi Kesalahan Verifikasi Token!'
				})
			}else{
				req.decoded = decoded
				next()
			}
		})
	} else {
		return res.status(403).json({
			success:false,
			message:'Tidak Ada User Token'
		})
	}
}