import express from 'express'
const router = express.Router()

const userController = require('../controller/user-controller')

router.get('/', userController.index)
router.post('/login', userController.login)
router.post('/register', userController.register)

module.exports = router;