import express from 'express'
const router = express.Router()

router.get('/', userController.index) // get all menu
router.get('/client', userController.client)
router.get('/admin', userController.admin)

module.exports = router;