import express from 'express'
const router = express.Router()

const produkController = require('../controller/produk-controller')
const uploadController = require('../controller/upload-controller')

const TokenCheck = require('../middlewares/token_check');
const rewriter = require('../middlewares/rewriter')

// const uploadMiddleware = require('../middlewares/upload')
// const upload = uploadMiddleware

/*
TokenCheck, 
TokenCheck, 
TokenCheck, 
*/

router.get('/', produkController.daftarproduk)
router.get('/:idkatg', produkController.produkByCategory)
router.post('/tambahproduk', produkController.tambahproduk)
router.post('/upload', uploadController.upload.single('produk'), rewriter, produkController.uploadProduk)

module.exports = router;

/*
192.168.10.1:4000/api/produk/					: menampilkan semua produk
192.168.10.1:4000/api/produk/all				: 
192.168.10.1:4000/api/produk/tambahproduk		: menambah produk
*/