import jwt from 'jsonwebtoken';

const tokenizer = (email, tokenSecret, tokenExpiresIn) => {
    return jwt.sign({ email: email }, tokenSecret, { expiresIn: tokenExpiresIn });
}

module.exports = tokenizer;