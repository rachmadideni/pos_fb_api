const nodemailer = require('nodemailer');

const sendMail = (mailOptions) => {

    const transporter = nodemailer.createTransport({
	    service: process.env.EMAIL_TRANSPORTER_SERVICE,
	    auth: {
			user:process.env.EMAIL_TRANSPORTER_AUTH_USER,
			pass:process.env.EMAIL_TRANSPORTER_AUTH_PASS
		}
	});

	transporter.sendMail(mailOptions, function(err, info) {
	    if (err){
			// console.log(err)
	    	return 0;
		}else{
			// console.log(info);
			return info.accepted.length; // 0 = fail >0 = success
		}
			
	})
}

module.exports = sendMail;