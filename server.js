require('babel-register')({presets: [ 'env' ]})
require('dotenv').config();

const http = require('http');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path')

const app = express()

const upload_dir = path.join(__dirname, 'upload/produk');

app.use(morgan("dev"));
app.use(express.urlencoded({ extended:false }));
app.use(express.json());
app.use(cors());
app.options('*', cors());

// route
const user = require('./routes/user')
const produk = require('./routes/produk')

app.use('/v1/api/user', user)
app.use('/v1/api/produk', produk)
app.use('/v1/api/produk/image', express.static(upload_dir))

const port = process.env.PORT || 4000;
const server = http.createServer(app);
server.listen(port);