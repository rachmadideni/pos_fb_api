import db from '../model'
import tokenizer from '../services/tokenizer'
import sendMail from '../services/sendMail';

exports.index = (req,res) => {
	return res.status(200).json({
		message:"welcome"
	})
}

// LOGIN
exports.login = (req, res) => {
	
	let user = req.body.username
	let pass = req.body.password

	db.models.user
	.findOne({ where: { username: user } })
	.then((result) => {

		if (result == null) {

			res.status(401).json({
				success:false,
				message:'user tdk ditemukan',
				token:null
			})

		}else{

			let token_secret = process.env.TOKEN_SECRET
			let token_exp = process.env.TOKEN_EXPIRE
			let token = tokenizer(user, token_secret, token_exp)

			// const nama_user = result.dataValues.username			
			res.status(200).json({
			    success:true,
			    message: 'user ditemukan',
			    token
			})
		}

	})
	.catch((err) => console.log(err))
	
}

// REGISTRASI
exports.register = (req, res) => {
	
	let tglreg = new Date()	
	let nmlgkp = req.body.nmlgkp
	let username = req.body.username
	let email = req.body.email
	let nomtel = req.body.nomtel
	let password = req.body.password

	db.models.user
	.findOrCreate({ where: { email: email }, 
	defaults: {
		tglreg,		
		nmlgkp,
		username,
		email,
		nomtel,
		password
	}})
	.spread((user, created) => {
	    console.log(user)
	    console.log(created)
	    if (created) {

	    	let token_secret = process.env.TOKEN_SECRET
			let token_exp = `15 minutes`
			let token_konfirmasi = tokenizer(email, token_secret, token_exp)

	    	// kirim email ke user
	    	let app_port = process.env.APP_PORT
	    	let subject = `[NOREPLY] Konfirmasi registrasi user `
	    	let body = `Hai Tn/Ny ${nmlgkp} anda menerima email ini karena telah melakukan registrasi<br/><br/>
			Silahkan klik link dibawah ini utk mengaktifkan user anda<br/><br/>
			<a href="http://localhost:${app_port}/user/konfirmasi?token=${token_konfirmasi}">Konfirmasi Registrasi</a>
	    	`
	    	const mailOptions = {
					from: process.env.EMAIL_DEFAULT_SENDER,
					to: email,
					subject: subject,
					html:body
				};

			sendMail(mailOptions);

	    	res.status(200).json({
	    		success:true,
	    		message:'User berhasil di registrasi',
	    		token:token_konfirmasi
	    	})

	    }else{
	    	res.status(401).json({
	    		success:false,
	    		message:'Terjadi Kesalahan. User gagal registrasi'
	    	})
	    }
	})
	.catch(err => console.log(err))

}
