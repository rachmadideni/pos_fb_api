import fs from 'fs'
import path from 'path'
import db from '../model'
import _ from 'lodash'
import multer from 'multer'
import sharp from 'sharp'// image compression fast than imagemagick
sharp.cache(false)

exports.index = (req,res) => {
	return res.status(200).json({
		message:"welcome"
	})
}

exports.daftarproduk = (req,res) => {
	
	
	let q = `select a.id,a.idkatg,b.kategori,a.nmprod,a.harga,1 as qty,imageUri
			from produk a
			inner join mst_kategori_produk b on a.idkatg=b.id
			order by b.kategori asc,a.nmprod asc`
	db.query(q,{ type:db.QueryTypes.SELECT })
	.then(results=>{
		
		if(!results){
			res.status(401).json({
				success:false,
				message:'tidak ada produk',
				count:_.sumBy(results,x => x ? 1 : 0),
				results:[]
			})
		}else{
			res.status(200).json({
				success:true,
				message:'produk ada',
				count:_.sumBy(results,x => x ? 1 : 0),
				results:results
			})
		}
	})
	.catch(err=>console.log(err));
}

exports.tambahproduk = (req,res) => {

	let idkatg = req.body.idkatg
	let nmprod = req.body.nmprod
	let harga = req.body.harga
	db.models.produk.findOrCreate({ where: { nmprod: '' }, 
		defaults: {
		    idkatg,
		    nmprod,
		    harga
		}
	}).spread((produk,created)=>{
		if(created){
			return res.status(200).json({
				success:true,
				message:'produk telah ditambahkan'
			})
		}else{
			return res.status(401).json({
				success:false,
				message:'terjadi kesalahan'
			})
		}
	})
	.catch(err=>console.log(err));	
}

exports.produkByCategory = (req,res) => {
	
	let idkatg = parseInt(req.params.idkatg,10)
	db.query('SELECT * FROM produk WHERE IDKATG=:idkatg',{
		replacements:{
			idkatg:idkatg
		},type:db.QueryTypes.SELECT
	})
	.then(results=>{
		if(!results){
			res.status(401).json({
				success:false,
				data:[]				
			})
		}else{
			res.status(200).json({
				success:true,
				data:results
			})
		}
	})
	.catch(err=>console.log(err))
}

exports.uploadProduk = (req,res) => {

	let idprod = 3; // test id 1
	let fileuri = req.body.fileuri;
	let filename = req.body.filename;
	let location = path.resolve(__dirname + '/../' + fileuri + filename) // direktori + namafile.ext
	let dir = path.resolve(__dirname + '/../' + fileuri) // direktori saja

	fs.stat(__dirname + '/../' + fileuri + filename,(err)=>{
		
		if(err){ console.log(err.code) }

		// ayo kompress file		
		// pake buffer

		sharp(location)
		.resize(200,200)
		.toBuffer()
		.then((buffer)=>{
			return new Promise((resolve,reject)=>{
				fs.writeFile(location,buffer,function(err){
					if(err){
						reject(err)
					}else{

					}
				})
			})
		})

	});

	db.query('UPDATE produk SET imageUri=:filename WHERE id=:idprod',{
		replacements:{
			idprod:idprod,
			filename:filename
		}	
	})
	.spread((results,metadata)=>{
		// console.log(results);
		if(results){			
			return res.status(200).json({
				success:true,
				message:"File Telah ter upload"
			})
		}else{
			return res.status(401).json({
				success:false,
				message:"File gagal upload"
			})
		}		
	})
	.catch(err => console.log(err))
}

exports.addOrder = (req, res) => {

	let idprod = req.body.idprod
	let nmprod = req.body.nmprod
	let harga = req.body.harga
	let qty = req.body.qty
	let disc = req.body.disc
	let total = req.body.total

	// transaksi (belum dipotong ppn)
	/*
		"id" => 1
		"idoutlet" => null
		"idmeja" => null
		"idinpt" => "1"
		"notran" => "1537846633813"
		"tgltra" => "2018-09-25"
		"jmtran" => 11.000 
		"ketera" => 'ORDER NO. 1537846633813'
	*/

	// transaksi detail
	/*
		"id"
		"idtran"
		"notran"
		"idprod"
		"idinpt"
		"tgltra"
		"qty"
		"jmtran"
		"diskon"
		"fldiskon"
	*/

	/*
	id 	idtran 	notran 			idprod 	idinpt 	tgltra 		qty 	jmtran 	diskon	fldiskon
	1	1		1537846633813	1		1		2018-09-25	1		5.000	1000	T
	2	1		1537846633813	2		1		2018-09-25	1		6.000	0		T

	*/

	/*
		
		modal update order
		=====================
		nama produk 	readonly
		qty 			quantity bisa dirubah default sesuai di data order 
		harga			readonly
		diskon			toggle diskon % atau diskon nominal

		
		tombol kurang quantity
		muncul dialog box konfirmasi ya atau tidak

		tombol tambah quantity
		muncul dialog box konfirmasi ya atau tidak		

	*/



}