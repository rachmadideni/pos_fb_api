const multer = require('multer')
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'upload/produk/')
  },
  filename: function (req, file, cb) {
  	const ext = file.mimetype.split('/')[1]
    cb(null, 'PRODUK_' + Date.now() + '.' + ext)
    // cb(null, 'PRODUK_' + Date.now() + '_' + file.originalname)
  }
})

const upload = multer({ storage: storage })

module.exports = { upload }

// PRODUK_IDOUTLET_1537840257879