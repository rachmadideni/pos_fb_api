module.exports = (sequelize,DataTypes) => {
	return sequelize.define('meja',{
		id:{
			type:DataTypes.INTEGER(11),
			allowNull:false,
			primaryKey:true,
			autoIncrement:true
		},
		nomor_meja:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		ketera:{
			type:DataTypes.STRING(255),
			allowNull:false
		}
	},{
		tableName:'ms_meja',
		timestamps:false
	});
}