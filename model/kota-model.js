module.exports = (sequelize,DataTypes) => {
	return sequelize.define('kota',{
		id:{
			type:DataTypes.INTEGER(11),
			allowNull:false,
			primaryKey:true,
			autoIncrement:true
		},
		idprop:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		nmkota:{
			type:DataTypes.STRING(255),
			allowNull:true
		}
	},{
		tableName:'ms_kota',
		timestamps:false
	});
}