module.exports = function(sequelize, DataTypes) {
  return sequelize.define('produk', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    idkatg: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    nmprod: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    harga: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    imageUri: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
  }, {
    tableName: 'produk',
    timestamps: false
  });
};