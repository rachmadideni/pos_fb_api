module.exports = (sequelize,DataTypes) => {
	return sequelize.define('provinsi',{
		id:{
			type:DataTypes.INTEGER(11),
			allowNull:false,
			primaryKey:true,
			autoIncrement:true
		},
		nmprov:{
			type:DataTypes.STRING(255),
			allowNull:true
		}
	},{
		tableName:'ms_provinsi',
		timestamps:false
	});
}