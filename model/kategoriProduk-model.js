module.exports = (sequelize,DataTypes) => {
	return sequelize.define('kategori_produk',{
		id:{
			type:DataTypes.INTEGER(11),
			allowNull:false,
			primaryKey:true,
			autoIncrement:true
		},
		kategori:{
			type:DataTypes.STRING(255),
			allowNull:true
		}
	},{
		tableName:'mst_kategori_produk',
		timestamps:false
	});
}