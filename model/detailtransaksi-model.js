module.exports = (sequelize,DataTypes) => {
	return sequelize.define('detail_transaksi',{
		id:{
			type:DataTypes.INTEGER(11),
			allowNull:false,
			primaryKey:true,
			autoIncrement:true
		},
		idtran:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		notran:{
			type:DataTypes.STRING(255),
			allowNull:false
		},
		idprod:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		idinpt:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		idprod:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		tgltra:{
			type: DataTypes.DATEONLY,
			allowNull:false	
		},		
		qty:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		jmtran:{
			type: DataTypes.DECIMAL(12,2),
			allowNull:false
		},		
		diskon:{
			type: DataTypes.DECIMAL(12,2),
			allowNull:false
		},
		fldiskon:{
			type: DataTypes.CHAR(1),
      		defaultValues:'T',
      		allowNull: true
		}
	},{
		tableName:'dt_transaksi_dtl',
		timestamps:false
	});
}