module.exports = (sequelize,DataTypes) => {
	return sequelize.define('transaksi',{
		id:{
			type:DataTypes.INTEGER(11),
			allowNull:false,
			primaryKey:true,
			autoIncrement:true
		},
		idoutlet:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		idmeja:{
			type:DataTypes.INTEGER(5),
			allowNull:false
		},
		idinpt:{
			type:DataTypes.INTEGER(11),
			allowNull:false
		},
		notran:{
			type:DataTypes.STRING(255),
			allowNull:false
		},
		tgltra:{
			type: DataTypes.DATEONLY,
			allowNull:false	
		},				
		jmtran:{
			type: DataTypes.DECIMAL(12,2),
			allowNull:false
		},ketera:{
			type:DataTypes.STRING(255),
			allowNull:false	
		}
	},{
		tableName:'dt_transaksi',
		timestamps:false
	});
}