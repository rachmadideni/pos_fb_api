module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tglreg:{
      type: DataTypes.DATEONLY,
      allowNull:false
    },
    tgaktif:{
      type: DataTypes.DATEONLY,
      allowNull:true
    },
    nmlgkp: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    password:{
      type: DataTypes.STRING(150),
      allowNull: true
    },
    nomtel:{
      type: DataTypes.STRING(150),
      allowNull: true
    },
    flaktif: {
      type: DataTypes.ENUM('Y','T'),
      defaultValue:'T',
      allowNull: true
    }
  }, {
    tableName: 'user',
    timestamps: false,
  });
};