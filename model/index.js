import Sequelize from 'sequelize'
import fs from 'fs'
const basename = 'index.js'
const sequelize = new Sequelize('pos', 'sa', 'masukbos123', { 
	host: 'localhost', 
	dialect: 'mssql', 
	logging: false,
	dialectOptions: {
        instanceName: 'MSSQLSERVER'
    } 
})

fs.readdirSync(__dirname).filter(file=>{ 	    
    return (file.indexOf('.') !==0) && (file.slice(-3) === '.js')  && (file !== basename)    
}).forEach(file=>{		
	sequelize.import(__dirname + '\\' + file);		
});

sequelize.sync({ force: false }).then(() => {
    
});

module.exports = sequelize;